﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Tam.MakeItJSON
{
	public class MvcApplication : HttpApplication
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.MapRoute(
					"Default",
					"",
					new { controller = "Default", action = "Index" }
			);
		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			RegisterRoutes(RouteTable.Routes);
		}
	}
}
