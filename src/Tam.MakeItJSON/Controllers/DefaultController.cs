﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Xml;
using Newtonsoft.Json;

namespace Tam.MakeItJSON.Controllers
{
	public class DefaultController : Controller
	{
		public ActionResult Index(string url = null, string callback = null)
		{
			var method = Request.HttpMethod.ToUpper();

			switch (method)
			{
				// If someone has just visited the site
				case "GET":
					if (url == null)
						return JsonpSuccess(Documentation, callback);
					break;

				// If someone has done an invalid POST request
				case "POST":
					if (Request.InputStream == null)
						return JsonpFailure("", callback);
					break;

				default:
					return JsonpFailure("Unsupported method");
			}

			try
			{
				Stream stream;

				if (method == "GET")
				{
					var request = WebRequest.CreateHttp(url);
					var response = (HttpWebResponse) request.GetResponse();
					stream = response.GetResponseStream();

					if (stream == null)
						return JsonpFailure(new { message = "Response stream is null - HTTP status code was " + response.StatusCode });
				}
				else
				{
					stream = Request.InputStream;
				}

				var doc = new XmlDocument();
				doc.Load(stream);

				return JsonpSuccess(doc, callback);
			}
			catch (Exception ex) { return JsonpFailure(new { message = ex.Message }, callback); }
		}

		#region Helpers
		
		private static ContentResult JsonpSuccess(object result, string callback = null)
		{
			return JsonpResult(new { success = true, result }, callback);
		}

		private static ContentResult JsonpFailure(object error, string callback = null)
		{
			return JsonpResult(new { success = false, error }, callback);
		}

		private static ContentResult JsonpResult(object data, string callback = null)
		{
			var result = JsonConvert.SerializeObject(data);
			var callbackValid = (callback != null && Regex.IsMatch(callback, "^[$A-Za-z_][0-9A-Za-z_$]*$"));

			if (callbackValid)
				result = string.Format("{0}({1});", callback, result);

			return new ContentResult
			{
				ContentType = "application/" + (callbackValid ? "javascript" : "json"),
				Content = result,
				ContentEncoding = Encoding.UTF8
			};
		}

		#endregion

		#region Documentation

		private static readonly object Documentation = new
		{
			message =
				"I am a JSON(P) API. I convert XML to JSON. Look at \"syntax\" for information as to how you can make requests to me.\r\n" +
				"\r\n" +
				"For now, I can only understand XML. Maybe one day my creator could teach me to convert some other formats.\r\n" +
				"\r\n" +
				"I was built by James Billingham, who is a director for ThreeAM Limited and a developer for Code Computerlove. If you want to get in touch with him, his details are as follows:\r\n" +
				"Twitter:\t@BillinghamJ\r\n" +
				"Website:\twww.jamesbillingham.com\r\n" +
				"Email:\tjames@jamesbillingham.com\r\n" +
				"\r\n" +
				"P.S.: if you're concerned about privacy or whatever - no need to worry, I only keep your XML in my memory for a few seconds.\r\n" +
				"If you'd prefer to run your own version of this service, ask James about it. He is considering open sourcing it.",
			syntax = new[]
			{
				new
				{
					method = "GET",
					description = "Outputs this message in all its lovely JSON awesomeness - and yes, everything supports callbacks",
					parameters = new[] {
						new { name = "callback", optional = true, description = "The Javascript function to return JSON to", regex = "^[$A-Za-z_][0-9A-Za-z_$]*$" }
					}
				},
				
				new
				{
					method = "GET",
					description = "Downloads the XML file/request you specified, converts it to JSON, then gives it to you - you lucky thing!",
					parameters = new[]
					{
						new { name = "url", optional = false, description = "The XML file to retrieve and convert", regex = ".*" },
						new { name = "callback", optional = true, description = "The Javascript function to return JSON to", regex = "^[$A-Za-z_][0-9A-Za-z_$]*$" }
					}
				},
				
				new
				{
					method = "POST",
					description = "Takes in XML-encoded data through the POST stream, converts it to JSON, then outputs it in the usual way. This supports JSONP callbacks too, but I'm not sure why you'd want to use it in this case...",
					parameters = new[]
					{
						new { name = "callback", optional = true, description = "The Javascript function to return JSON to", regex = "^[$A-Za-z_][0-9A-Za-z_$]*$" }
					}
				}
			}
		};

		#endregion
	}
}
